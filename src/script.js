const images = document.querySelectorAll('.imageSlider img'),
  buttons = document.querySelectorAll('.btnSlider'),
  btnMenu = document.querySelector('#btnMenu'),
  menus = document.querySelector('.menus'),
  changeMenus = menus.querySelectorAll('li'),
  itens = document.querySelectorAll('.products > .coffees, .products .cakes, .products .iceCream'),
  imageSlider = document.querySelector('.imageSlider'),
  address = document.querySelector('.address'),
  maps = document.querySelector('.maps'),
  btnSlider = document.querySelectorAll('.btnLeft, .btnRight'),
  divSlider = document.querySelectorAll('.divLeft, .divRight'),
  search = document.querySelector('.searchCoffee');

for (let i = 0; i < images.length; i++) {
  buttons[i].onclick = () => {
    change(i);
    clearInterval(interval);
    index = i;
    interval = setInterval(() => {
      if (imageSlider.style.height !== '0px') {
        change(index++)
        index = index === images.length ? 0 : index;
      }
    }, 5000)
  }
}

function change(i) {
  images[i].setAttribute('class', 'currentImage');
  buttons[i].setAttribute('class', 'btnSlider selected');

  images.forEach(image => {
    if (image !== images[i]) image.removeAttribute('class')
  });
  buttons.forEach(button => {
    if (button !== buttons[i]) button.setAttribute('class', 'btnSlider')
  })
}

function btnClick(num) {
  return () => {
    if (imageSlider.style.height !== '0px') {
      if (num === 1) index = (index === images.length - 1) ? 0 : index + num
      else index = (index === 0) ? images.length - 1 : index + num
      change(index);
      clearInterval(interval);
      interval = setInterval(() => {
        if (imageSlider.style.height !== '0px') {
          index = index === images.length - 1 ? 0 : index + 1;
          change(index)
        }
      }, 5000)
    }
  }
}

btnSlider[0].onclick = btnClick(-1);
btnSlider[1].onclick = btnClick(1);
divSlider[0].onclick = btnClick(-1);
divSlider[1].onclick = btnClick(1);

document.onkeyup = e => {
  if (e.which === 37)
    btnSlider[0].click()
  else if (e.which === 39)
    btnSlider[1].click()
}

let index = 0,
  interval = setInterval(() => {
    if (imageSlider.style.height !== '0px') {
      index = index === images.length - 1 ? 0 : index + 1;
      change(index)
    }
  }, 5000);

btnMenu.onclick = () => {
  if (menus.style.height === '180px')
    menus.style.height = '0'
  else menus.style.height = '180px'
}

changeMenus[0].onclick = () => {
  menus.style.height = '0';
  imageSlider.style.height = '350px';
  if (innerWidth >= 500) imageSlider.style.marginTop = '10px'
  else imageSlider.style.marginTop = '0'
  changeMenus.forEach(button => button.setAttribute('class', 'menusItem'));
  changeMenus[0].setAttribute('class', 'menusItem actived');
  address.style.display = 'block';
  maps.style.height = '450px';
  maps.style.marginBottom = '10px';
  address.style.marginTop = '10px';
  showImages();
  updateList()
}

function showImages() {
  const num = parseInt(innerWidth / 304);
  itens.forEach(item => {
    item.style.display = 'none'
  });
  let currentNum = 0;
  itens.forEach(item => {
    if (item.getAttribute('class').includes('coffees')) {
      if (currentNum < num) {
        item.addEventListener('click', () => {
          if (!changeMenus[1].getAttribute('class').includes('actived')) {
            scrollTo(0, 0);
            changeMenus[1].click()
          }
        })
        currentNum++;
        item.style.display = 'inline-table';
        item.style.cursor = 'pointer'
      }
    } else if (item.getAttribute('class').includes('cakes')) {
      if (currentNum < num * 2) {
        item.addEventListener('click', () => {
          if (!changeMenus[2].getAttribute('class').includes('actived')) {
            scrollTo(0, 0);
            changeMenus[2].click()
          }
        })
        currentNum++;
        item.style.display = 'inline-table';
        item.style.cursor = 'pointer'
      }
    } else if (item.getAttribute('class').includes('iceCream')) {
      if (currentNum < num * 3) {
        item.addEventListener('click', () => {
          if (!changeMenus[3].getAttribute('class').includes('actived')) {
            scrollTo(0, 0);
            changeMenus[3].click()
          }
        })
        currentNum++;
        item.style.display = 'inline-table';
        item.style.cursor = 'pointer'
      }
    }
  })
}

showImages();

for (let i = 1; i < changeMenus.length; i++)
  changeMenus[i].onclick = () => {
    menus.style.height = '0';
    imageSlider.style.height = '0';
    if (innerWidth >= 500)
      imageSlider.style.marginTop = '0';
    changeMenus.forEach(button => button.setAttribute('class', 'menusItem'));
    changeMenus[i].setAttribute('class', 'menusItem actived');
    address.style.display = 'none';
    maps.style.height = '0';
    maps.style.marginBottom = '0';
    address.style.marginTop = '0';
    itens.forEach(item => {
      if (i === 1 && !item.className.includes('coffees'))
        item.style.display = 'none'
      else if (i === 2 && !item.className.includes('cakes'))
        item.style.display = 'none'
      else if (i === 3 && !item.className.includes('iceCream'))
        item.style.display = 'none'
      else {
        item.style.cursor = 'default';
        item.style.display = 'inline-table'
      }
    });
    updateList()
  }

function removeUTF8(text = String) {
  const none = 'aaaaeeeeiiiioooouuuu',
    utf8 = 'áàãâéèẽêíìĩîóòõôúùũû';

  for (let i = 0; i < none.length; i++)
    while (text.indexOf(utf8[i]) !== -1)
      text = text.replace(utf8[i], none[i])

  return text
}

search.onkeyup = e => {
  if (e.which === 13) {
    itens.forEach(item => {
      if (item.style.display !== 'none' && removeUTF8(search.value.trim().toLowerCase()) === removeUTF8(item.getAttribute('data-name').toLowerCase())) {
        let y = scrollY;
        const toItem = setInterval(() => {
          if (y <= item.offsetTop)
            scrollTo(0, y += 40)
          else {
            scrollTo(0, item.offsetTop);
            clearInterval(toItem)
          }
        }, 1);
        search.value = '';
        search.style.display = 'none'
      }
    })
  }
}

function toTop() {
  let y = scrollY;
  const toTop = setInterval(() => {
    if (y > 0) scrollTo(0, y -= 50)
    else clearInterval(toTop)
  }, 1);
}

document.querySelector('.search').onclick = () => {
  if (search.style.display === 'block')
    search.style.display = 'none'
  else {
    search.select();
    search.style.display = 'block'
  }
}

function updateList() {
  let html = '';
  for (let i = 0; i < itens.length; i++)
    if (itens[i].style.display !== 'none')
      html += `<option value="${itens[i].getAttribute('data-name')}"></option>`

  document.querySelector('#itens').innerHTML = html
}

updateList()